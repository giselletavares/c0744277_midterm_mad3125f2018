package com.giselletavares.c0744277_midterm_mad3125f2018.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;

import com.giselletavares.c0744277_midterm_mad3125f2018.R;

public class LoginActivity extends AppCompatActivity {

    private EditText mTxtEmail;
    private EditText mTxtPassword;
    private Button mBtnLogin;
    private Switch mSwRememberMe;

    private SharedPreferences mSharedPreferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mTxtEmail = findViewById(R.id.txtEmail);
        mTxtPassword = findViewById(R.id.txtPassword);
        mBtnLogin = findViewById(R.id.btnLogin);
        mSwRememberMe = findViewById(R.id.swRememberMe);

        mSwRememberMe.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                SharedPreferences.Editor mEditor = mSharedPreferences.edit();
                mEditor.putString("userEmail", mTxtEmail.getText().toString());
                mEditor.putString("userPassword", mTxtPassword.getText().toString());
                mEditor.apply();

            }
        });

        mBtnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginValidation(mTxtEmail.getText().toString(), mTxtPassword.getText().toString());
            }
        });

        getSharedPreferences();
    }

    private void getSharedPreferences(){
        mSharedPreferences = getSharedPreferences("userDetails", MODE_PRIVATE);

        String userEmail = mSharedPreferences.getString("userEmail", null);
        String userPassword = mSharedPreferences.getString("userPassword", null);

        if(userEmail != null && userPassword != null) {
            mTxtEmail.setText(userEmail);
            mTxtPassword.setText(userPassword);
        }
    }

    private void loginValidation(String email, String password){

        if(email.equals("admin@admin.com") && password.equals("123456")) {

            startActivity(new Intent(LoginActivity.this, ElectricityBillActivity.class));
            finish();

        } else {

            AlertDialog.Builder mBuilder = new AlertDialog.Builder(LoginActivity.this);
            String regexpEmail = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";

            mBuilder.setTitle("Login Error");
            if(email.isEmpty() || password.isEmpty()) {
                mBuilder.setMessage("Email/Password can't be empty.");
            } else if(!email.matches(regexpEmail)) {
                mBuilder.setMessage("Inform a valid email.");
            } else {
                mBuilder.setMessage("Email/Password wrong.");
            }

            mBuilder.setNegativeButton("Try again", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.cancel();
                }
            });

            AlertDialog mDialogLabels = mBuilder.create();
            mDialogLabels.show();

        }

    }
}
