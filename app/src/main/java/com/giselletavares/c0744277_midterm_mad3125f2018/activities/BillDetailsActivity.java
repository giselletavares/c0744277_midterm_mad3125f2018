package com.giselletavares.c0744277_midterm_mad3125f2018.activities;

import android.arch.persistence.room.Room;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.giselletavares.c0744277_midterm_mad3125f2018.R;
import com.giselletavares.c0744277_midterm_mad3125f2018.models.AppDatabase;
import com.giselletavares.c0744277_midterm_mad3125f2018.models.ElectricityBill;
import com.giselletavares.c0744277_midterm_mad3125f2018.utils.Formatting;

import java.text.Normalizer;
import java.text.SimpleDateFormat;

public class BillDetailsActivity extends AppCompatActivity {

    public static AppDatabase sAppDatabase;

    private TextView mLblCustomerEmail;
    private TextView mLblCustomerID;
    private TextView mLblCustomerName;
    private TextView mLblCustomerGender;
    private TextView mLblUnitConsumed;
    private TextView mLblBillDate;
    private TextView mLblTotalAmount;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bill_details);

        mLblCustomerEmail = findViewById(R.id.lblCustomerEmail);
        mLblCustomerID = findViewById(R.id.lblCustomerID);
        mLblCustomerName = findViewById(R.id.lblCustomerName);
        mLblCustomerGender = findViewById(R.id.lblCustomerGender);
        mLblUnitConsumed = findViewById(R.id.lblUnitConsumed);
        mLblBillDate = findViewById(R.id.lblBillDate);
        mLblTotalAmount = findViewById(R.id.lblTotalAmount);

        // for Back button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        // DATABASE
        sAppDatabase = Room.databaseBuilder(getApplicationContext(), AppDatabase.class, "electricitybilldb")
                .allowMainThreadQueries() // it will allow the database works on the main thread
                .fallbackToDestructiveMigration() // because i wont implement now migrations
                .build();

        ElectricityBill bill = BillDetailsActivity.sAppDatabase.mElectricityBillDAO().getLastBill();

        Formatting formatting = new Formatting();

        mLblCustomerEmail.setText("Email: " + bill.getCustomerEmail());
        mLblCustomerID.setText("ID: " + bill.getCustomerId());
        mLblCustomerName.setText("Name: " + bill.getCustomerName());
        mLblCustomerGender.setText("Gender: " + bill.getGender());
        mLblUnitConsumed.setText("Unit Consumed: " + formatting.getKwhFormatter(bill.getUnitConsumed()));
        mLblBillDate.setText("Bill Date: " + formatting.getDateFormatter(bill.getBillDate()));
        mLblTotalAmount.setText(formatting.getCurrencyFormatter(bill.getTotalBillAmount()));

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.electricity_bill_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mnMyBills:
                startActivity(new Intent(BillDetailsActivity.this, ListBillActivity.class));
                finish();
                break;
            case R.id.mnLogout:
                startActivity(new Intent(BillDetailsActivity.this, LoginActivity.class));
                finish();
                break;
            case android.R.id.home: // for back button
                startActivity(new Intent(BillDetailsActivity.this, ElectricityBillActivity.class));
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
