package com.giselletavares.c0744277_midterm_mad3125f2018.activities;

import android.app.DatePickerDialog;
import android.arch.persistence.room.Room;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.giselletavares.c0744277_midterm_mad3125f2018.R;
import com.giselletavares.c0744277_midterm_mad3125f2018.models.AppDatabase;
import com.giselletavares.c0744277_midterm_mad3125f2018.models.ElectricityBill;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class ElectricityBillActivity extends AppCompatActivity implements View.OnClickListener {

    public static AppDatabase sAppDatabase;

    private EditText mTxtCustomerEmail;
    private EditText mTxtCustomerName;
    private EditText mTxtCustomerID;
    private RadioGroup mRbCustomerGender;
    private EditText mUnitConsumed;
    private Button mDatePickerButton;
    private TextView mDateSelected;
    private Button mCalculateButton;

    private DatePickerDialog mDatePickerDialog;

    private int mYear;
    private int mMonth;
    private int mDayOfMonth;
    private Calendar mCalendar;

    private SharedPreferences mSharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_electricity_bill);

        mTxtCustomerEmail = findViewById(R.id.txtCustomerEmail);
        mTxtCustomerName = findViewById(R.id.txtCustomerName);
        mTxtCustomerID = findViewById(R.id.txtCustomerID);
        mRbCustomerGender = findViewById(R.id.rbGenderGroup);
        mUnitConsumed = findViewById(R.id.txtUnitConsumed);
        mDatePickerButton = findViewById(R.id.btnDatePicker);
        mDateSelected = findViewById(R.id.lblDateSelected);
        mCalculateButton = findViewById(R.id.btnCalculate);

        // DATABASE
        sAppDatabase = Room.databaseBuilder(getApplicationContext(), AppDatabase.class, "electricitybilldb")
                .allowMainThreadQueries() // it will allow the database works on the main thread
                .fallbackToDestructiveMigration() // because i wont implement now migrations
                .build();

        mDatePickerButton.setOnClickListener(this);
        mCalculateButton.setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.electricity_bill_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mnMyBills:
                startActivity(new Intent(ElectricityBillActivity.this, ListBillActivity.class));
                finish();
                break;
            case R.id.mnLogout:
                startActivity(new Intent(ElectricityBillActivity.this, LoginActivity.class));
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.btnDatePicker:
                mCalendar = Calendar.getInstance();
                mCalendar.add(Calendar.DAY_OF_YEAR, 1);
                mYear = mCalendar.get(Calendar.YEAR);
                mMonth = mCalendar.get(Calendar.MONTH);
                mDayOfMonth = mCalendar.get(Calendar.DAY_OF_MONTH);

                mDatePickerDialog = new DatePickerDialog(ElectricityBillActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                        mDateSelected.setText(day + "/" + (month + 1) + "/" + year);
                    }
                }, mYear, mMonth, mDayOfMonth);
                mDatePickerDialog.getDatePicker().setMinDate(mCalendar.getTimeInMillis());
                mDatePickerDialog.show();
                break;

            case R.id.btnCalculate:

                Boolean isValid = fieldsValidation(
                        mTxtCustomerEmail.getText().toString(),
                        mTxtCustomerID.getText().toString(),
                        mTxtCustomerName.getText().toString(),
                        mUnitConsumed.getText().toString(),
                        mDateSelected.getText().toString()
                );

                if (isValid) {

                    // preparing gender
                    String gender = "";
                    switch (mRbCustomerGender.getCheckedRadioButtonId()) {
                        case R.id.rbFemale:
                            gender = "Female";
                            break;
                        case R.id.rbMale:
                            gender = "Male";
                            break;
                        case R.id.rbOther:
                            gender = "Other";
                            break;
                    }

                    // preparing bill ID
                    SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
                    Date currentDateTime = new Date();

                    // preparing unit Consumed to Double
                    double unitConsumed = Double.parseDouble(mUnitConsumed.getText().toString());

                    // preparing Bill Date
                    Date billDate = currentDateTime;
                    try {
                        billDate = new SimpleDateFormat("dd/MM/yyyy").parse(mDateSelected.getText().toString());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    ElectricityBill bill = new ElectricityBill();
                    bill.setBillId(formatter.format(currentDateTime));
                    bill.setUnitConsumed(unitConsumed);
                    bill.setBillDate(billDate);
                    bill.setTotalBillAmount(calculateTheBill(unitConsumed));
                    bill.setCustomerEmail(mTxtCustomerEmail.getText().toString());
                    bill.setCustomerId(mTxtCustomerID.getText().toString());
                    bill.setCustomerName(mTxtCustomerName.getText().toString());
                    bill.setGender(gender);

                    // save data in the database
                    ElectricityBillActivity.sAppDatabase.mElectricityBillDAO().addBill(bill);

                    // go to details
                    startActivity(new Intent(ElectricityBillActivity.this, BillDetailsActivity.class));
                    finish();
                }
                break;
        }

    }

    private Boolean fieldsValidation(String email, String id, String name, String unitConsumed, String billDate) {

        AlertDialog.Builder mBuilder = new AlertDialog.Builder(ElectricityBillActivity.this);
        String regexpId = "^[C]{1}\\d{9}";
        String regexName = "^([a-zA-Z]+\\s)*[a-zA-Z]+$";
        String regexEmail = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";

        double unitConsumedConverted;
        if(unitConsumed.isEmpty()) {
            unitConsumedConverted = 0.00;
        } else {
            unitConsumedConverted = Double.parseDouble(unitConsumed);
        }

        mBuilder.setTitle("Error");
        if(email.isEmpty() || id.isEmpty() || name.isEmpty() || unitConsumed.isEmpty() || billDate.isEmpty()) {
            mBuilder.setMessage("The fields can't be empty.");
        } else if(unitConsumedConverted <= 0.00) {
            mBuilder.setMessage("The unit consumed must be greater than 0.");
        } else if(!email.matches(regexEmail)) {
            mBuilder.setMessage("The Email must be valid.");
        } else if(!id.matches(regexpId)) {
            mBuilder.setMessage("The ID must start with C and 9 numerical characters.");
        } else if(!name.matches(regexName)) {
            mBuilder.setMessage("The Name must contain only letters and one space between the name.");
        } else {
            return true;
        }

        mBuilder.setNegativeButton("Try again", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });

        AlertDialog mDialogLabels = mBuilder.create();
        mDialogLabels.show();

        return false;
    }

    private Double calculateTheBill(Double unitConsumed){
//
//        - For 1st 100 unit $ 0.75 per unit
//        - For next 150 units $ 1.25 per unit
//        - For next 200 units $ 1.75 per unit
//        - For all above 450 units $ 2.25 per unit

        if(unitConsumed <= 100) {
            return unitConsumed * 0.75;
        } else if(unitConsumed <= 250) {
            return 75 + ((unitConsumed - 100) * 1.25);
        } else if(unitConsumed <= 450) {
            return 262.5 + ((unitConsumed - 250) * 1.75);
        }
        return 612.5 + ((unitConsumed - 450) * 2.25);

    }
}
