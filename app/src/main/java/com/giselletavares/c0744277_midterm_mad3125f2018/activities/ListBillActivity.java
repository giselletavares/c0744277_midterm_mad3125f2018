package com.giselletavares.c0744277_midterm_mad3125f2018.activities;

import android.arch.persistence.room.Room;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.TextView;

import com.giselletavares.c0744277_midterm_mad3125f2018.R;
import com.giselletavares.c0744277_midterm_mad3125f2018.adapters.BillAdapter;
import com.giselletavares.c0744277_midterm_mad3125f2018.models.AppDatabase;
import com.giselletavares.c0744277_midterm_mad3125f2018.models.ElectricityBill;
import com.giselletavares.c0744277_midterm_mad3125f2018.utils.Formatting;

import java.util.ArrayList;
import java.util.List;

public class ListBillActivity extends AppCompatActivity {

    public static AppDatabase sAppDatabase;

    private ListView mLstBills;
    private BillAdapter mBillAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_bill);

        mLstBills = findViewById(R.id.lstBills);

        // for Back button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        // DATABASE
        sAppDatabase = Room.databaseBuilder(getApplicationContext(), AppDatabase.class, "electricitybilldb")
                .allowMainThreadQueries() // it will allow the database works on the main thread
                .fallbackToDestructiveMigration() // because i wont implement now migrations
                .build();

        List<ElectricityBill> bills = BillDetailsActivity.sAppDatabase.mElectricityBillDAO().getBills();

        ArrayList<ElectricityBill> billsList = new ArrayList<>();

        for(ElectricityBill bill : bills) {
            billsList.add(bill);
        }

        mBillAdapter = new BillAdapter(this, billsList);
        mLstBills.setAdapter(mBillAdapter);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.electricity_bill_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mnMyBills:
                startActivity(new Intent(ListBillActivity.this, ListBillActivity.class));
                finish();
                break;
            case R.id.mnLogout:
                startActivity(new Intent(ListBillActivity.this, LoginActivity.class));
                finish();
                break;
            case android.R.id.home: // for back button
                startActivity(new Intent(ListBillActivity.this, ElectricityBillActivity.class));
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
