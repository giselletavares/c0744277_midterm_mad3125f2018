package com.giselletavares.c0744277_midterm_mad3125f2018.utils;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Formatting {

    Locale locale = new Locale("en", "CA");
    NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(locale);
    SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy");

    public Formatting() {
    }

    public String getCurrencyFormatter(Double value) {
        return currencyFormatter.format(value);
    }

    public String getDateFormatter(Date value) {
        return dateFormatter.format(value);
    }

    public String getKwhFormatter(Double value) {
        return value + "kWh";
    }

}
