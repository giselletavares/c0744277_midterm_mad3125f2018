package com.giselletavares.c0744277_midterm_mad3125f2018.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.giselletavares.c0744277_midterm_mad3125f2018.R;
import com.giselletavares.c0744277_midterm_mad3125f2018.models.ElectricityBill;
import com.giselletavares.c0744277_midterm_mad3125f2018.utils.Formatting;

import java.util.List;

public class BillAdapter extends ArrayAdapter<ElectricityBill> {

    private List<ElectricityBill> mElectricityBillArrayList;
    private Context mContext;

    public BillAdapter(Context context, List<ElectricityBill> electricityBill) {
        super(context, 0, electricityBill);

        this.mElectricityBillArrayList = electricityBill;
        this.mContext = context;
    }

    @Override
    public int getCount() {
        return mElectricityBillArrayList.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        BillViewHolder mHolder;

        if(convertView == null) {
            convertView = LayoutInflater
                    .from(mContext)
                    .inflate(R.layout.item_bill_list, parent, false);
            mHolder = new BillViewHolder(convertView);
            convertView.setTag(mHolder);
        } else {
            mHolder = (BillViewHolder) convertView.getTag();
        }

        Formatting formatting = new Formatting();

        ElectricityBill currentBill = mElectricityBillArrayList.get(position);

        mHolder.mLblCustomerEmail.setText("Email: " + currentBill.getCustomerEmail());
        mHolder.mLblCustomerID.setText("ID: " + currentBill.getCustomerId());
        mHolder.mLblCustomerName.setText("Name: " + currentBill.getCustomerName());
        mHolder.mLblCustomerGender.setText("Gender: " + currentBill.getGender());
        mHolder.mLblUnitConsumed.setText("Unit Consumed: " + formatting.getKwhFormatter(currentBill.getUnitConsumed()));
        mHolder.mLblBillDate.setText("Bill Date: " + formatting.getDateFormatter(currentBill.getBillDate()));
        mHolder.mLblTotalAmount.setText("Total amount: " + formatting.getCurrencyFormatter(currentBill.getTotalBillAmount()));

        return convertView;
    }

    private class BillViewHolder {

        private TextView mLblCustomerEmail;
        private TextView mLblCustomerID;
        private TextView mLblCustomerName;
        private TextView mLblCustomerGender;
        private TextView mLblUnitConsumed;
        private TextView mLblBillDate;
        private TextView mLblTotalAmount;

        public BillViewHolder(View view) {
            mLblCustomerEmail = view.findViewById(R.id.lblCustomerEmail);
            mLblCustomerID = view.findViewById(R.id.lblCustomerID);
            mLblCustomerName = view.findViewById(R.id.lblCustomerName);
            mLblCustomerGender = view.findViewById(R.id.lblCustomerGender);
            mLblUnitConsumed = view.findViewById(R.id.lblUnitConsumed);
            mLblBillDate = view.findViewById(R.id.lblBillDate);
            mLblTotalAmount = view.findViewById(R.id.lblTotalAmount);
        }
    }

}
