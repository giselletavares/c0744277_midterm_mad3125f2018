package com.giselletavares.c0744277_midterm_mad3125f2018.models;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;

@Database(entities = {ElectricityBill.class}, version = 2)
@TypeConverters({DataTypeConverter.class})
public abstract class AppDatabase extends RoomDatabase {

    public abstract ElectricityBillDAO mElectricityBillDAO();
    
}
