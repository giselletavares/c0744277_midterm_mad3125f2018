package com.giselletavares.c0744277_midterm_mad3125f2018.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.text.SimpleDateFormat;
import java.util.Date;

@Entity(tableName = "bills")
public class ElectricityBill {

    @PrimaryKey
    @NonNull
    private String billId;

    @NonNull
    private Date billDate;

    @NonNull
    private Double unitConsumed;

    @NonNull
    private Double totalBillAmount;

    @NonNull
    private String customerId;

    @NonNull
    private String customerName;

    @NonNull
    private String customerEmail;

    @NonNull
    private String gender;



    @NonNull
    public String getBillId() {
        return billId;
    }

    public void setBillId(@NonNull String billId) {
        this.billId = billId;
    }

    @NonNull
    public Date getBillDate() {
        return billDate;
    }

    public void setBillDate(@NonNull Date billDate) {
        this.billDate = billDate;
    }

    @NonNull
    public Double getUnitConsumed() {
        return unitConsumed;
    }

    public void setUnitConsumed(@NonNull Double unitConsumed) {
        this.unitConsumed = unitConsumed;
    }

    @NonNull
    public Double getTotalBillAmount() {
        return totalBillAmount;
    }

    public void setTotalBillAmount(@NonNull Double totalBillAmount) {
        this.totalBillAmount = totalBillAmount;
    }

    @NonNull
    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(@NonNull String customerId) {
        this.customerId = customerId;
    }

    @NonNull
    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(@NonNull String customerName) {
        this.customerName = customerName;
    }

    @NonNull
    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(@NonNull String customerEmail) {
        this.customerEmail = customerEmail;
    }

    @NonNull
    public String getGender() {
        return gender;
    }

    public void setGender(@NonNull String gender) {
        this.gender = gender;
    }
}
