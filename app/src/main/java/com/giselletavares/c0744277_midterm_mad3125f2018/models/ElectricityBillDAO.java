package com.giselletavares.c0744277_midterm_mad3125f2018.models;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface ElectricityBillDAO {

    @Insert
    public void addBill(ElectricityBill electricityBill);

    @Query("SELECT * FROM bills")
    public List<ElectricityBill> getBills();

    @Query("SELECT * FROM bills ORDER BY billId DESC LIMIT 1")
    public ElectricityBill getLastBill();

}
